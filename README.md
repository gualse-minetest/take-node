# How to use

Make sure that you have `creative` privilege.
Open chat and enter `/tn`.
You also can press Special/AUX key if it is enabled in the settings. (`take_node_aux_bind=true` for server)

# ATTENTION!

Not all nodes can be taken! If the taken block looks ugly **ELIMINATE** it!
