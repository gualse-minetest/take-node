function take_node(player)
  local ray_start = vector.add(player:getpos(), {x=0,y=1.5,z=0})
  local ray_end = vector.add(ray_start, vector.multiply(player:get_look_dir(), 10))

  local ray = minetest.raycast(ray_start, ray_end, false, true)

  for pointed_thing in ray do
    local node = minetest.get_node(pointed_thing.under)

    if node.name ~= "air" and node.name ~= "ignore" and not string.match(node.name, "wielded_light:") then
        player.set_wielded_item(player, ItemStack(node))
      return
    end
  end
end

minetest.register_chatcommand("tn", {
  description = "Take node to inventory",
  privs = {creative=true},
  func = function(name)
    take_node(minetest.get_player_by_name(name))
  end
})

if minetest.settings:get_bool("take_node_aux_bind", false) then
  minetest.register_globalstep(function(dtime)
    local players = minetest.get_connected_players()

    for i = 1, #players do
      if players[i]:get_player_control().aux1 and minetest.check_player_privs(players[i], {creative=true}) then
        take_node(players[i])
      end
    end
  end)
end